<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Room_model extends CI_Model
{
    function getroom()
    {
        // mengambil semua pengguna
        $query = $this->db->query("select * from ruangan");

        if($query->num_rows()>0){
            return $query->result();
        }  else {
            return "";
        }
    }
    
    function getroomByID($id)
    {
        // mengambil data pengguna berdasarkan ID
        $query = $this->db->query("select * from ruangan where id_ruangan = '$id'");

        if($query->num_rows()>0){
            return $query->row();
        }  else {
            return "";
        }
    }

    public function tambah($data)
    {
        $query = $this->db->insert('ruangan', $data);
        if($query)
        {
            $result = array('status'=>true);
            return $result;
        }else {
            $result = array('status'=>false, 'error' => "Data gagal di tambah".$this->db->error());
            return $result;
        }
    }

    public function tambahPesanan($data)
    {
        $query = $this->db->insert('pesanan', $data);
        if($query)
        {
            $result = array('status'=>true);
            return $result;
        }else {
            $result = array('status'=>false, 'error' => "Data gagal di tambah".$this->db->error());
            return $result;
        }
    }

    public function updatePesanan($data,$id)
    {
        $this->db->where('id_pesanan', $id);
        $query = $this->db->update('pesanan', $data);
        if($query){
            redirect('home/pesanan');
        }  else {
            redirect('home/pesanan?err=Gagal');
        }
    }

    public function tambahDetailorder($data)
    {
        $query = $this->db->insert('pesanan_detail', $data);
        if($query)
        {
            $result = array('status'=>true);
            return $result;
        }else {
            $result = array('status'=>false, 'error' => "Data gagal di tambah".$this->db->error());
            return $result;
        }
    }

    function getAllorder()
    {
        // mengambil semua pengguna
        $query = $this->db->query("select * from pesanan");

        if($query->num_rows()>0){
            return $query->result();
        }  else {
            return "";
        }
    }

    function getPesanan()
    {
        // mengambil semua pengguna
        $query = $this->db->query("select * from pesanan left join ruangan on ruangan.id_ruangan = pesanan.id_ruangan left join pengguna on pengguna.id_pengguna=pesanan.id_pengguna");

        if($query->num_rows()>0){
            return $query->result();
        }  else {
            return "";
        }
    }

    function selesaiPesanan($id)
    {
        //$pesanan = $this->db->query("select * from pesanan where id_ruangan='$id'")->row();

        $query = $this->db->query("update ruangan set status_ruangan='tersedia' where id_ruangan = '$id';");

        if($query){
            redirect('home/');
        }  else {
            redirect('home/?err=Gagal');
        }
    }

}