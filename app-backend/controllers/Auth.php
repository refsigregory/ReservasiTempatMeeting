<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

    public function __construct()
        {
            parent::__construct();

            // auto load model
            $this->load->model('auth_model');
        }

        public function index()
        {
                    // memanggil fungsi cek  login
                    $this->auth_model->check();
        }


        public function checkLogin()
        {
            // cek login
                
            $this->auth_model->logged_in();
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            // memanggil fungsi cek login
            if($this->auth_model->loginPengguna($username, $password))
            {

                redirect('home', 'refresh');
            }else if($this->auth_model->loginAdmin($username, $password)) {

                redirect('home', 'refresh');
            }else {
                // login gagal
                $msg = "Login Gagal!";
                $this->session->set_flashdata('msg', array('type' => 'error', 'message' => $msg));
                redirect('home/login', 'refresh');
            }
        }

        public function logout()
        {
            $this->session->sess_destroy();
            redirect('home/login');
        }
}