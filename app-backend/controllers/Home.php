<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

        public function __construct()
        {
                parent::__construct();

                $this->load->model('auth_model');
                $this->load->model('room_model');
        }

    public function index()
    {
                $head['title'] = "Reservasi Tempat Meeting";
                $head['loginPage'] = false;
                $this->load->view('_templates/home_header', $head);

                $data['ruangan'] = $this->room_model->getroom();

                $data['page'] = 'home_view';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                if(isset($_GET['view']))
                { // menampilkan modal lihat harga
                    $data['detail'] = $this->room_model->getroomByID($this->input->get('view'));
                    $data['a'] = 'aa';
                    $this->load->view('show_detail_room', $data);
                }

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }

    public function tambah_ruangan()
    {
                $head['title'] = "Reservasi Tempat Meeting";
                $head['loginPage'] = false;
                $this->load->view('_templates/home_header', $head);

                $data['ruangan'] = $this->room_model->getroom();

                $data['page'] = 'tambah_ruangan_view';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                if(isset($_GET['view']))
                { // menampilkan modal lihat harga
                    $data['detail'] = $this->room_model->getroomByID($this->input->get('view'));
                    $data['a'] = 'aa';
                    $this->load->view('show_detail_room', $data);
                }

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }

    public function tambahRuangan()
    {
            $config['upload_path']          = './app-uploads/';
            $config['allowed_types']        = 'gif|jpg|jpeg|png';
            $config['encrypt_name']         = true;
            $config['max_size']             = '1000'; //kilobyte
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('gambar'))
            {
                    $error = $this->upload->display_errors();
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
                    $gambar = $data['upload_data']['file_name'];
            }

            $data = $this->input->post();
            $jumlah = $data['jumlah_orang'];

            unset($data['gambar']);

            foreach($data as $item=>$value){
                if($value == ""){
                    $err[] = $item . " tidak boleh kosong!";
                }
            }

            //$data2 = array('tanggal_pemesanan' => date("Y/m/d")
            //    );
            //$data = array_merge($data,$data2);
            if(isset($gambar)){
                if(!isset($err)){
                     $insert = $this->room_model->tambah($data);

                    if($insert['status']){
                        $this->session->set_flashdata('msg', 'Sukses');
                        redirect('home?msg=Pesanan Berhasil Diproses');
                    }else {
                        $this->session->set_flashdata('error', $insert['error']);
                        redirect('home/?error='.$insert['error']);
                    }
                }else {
                    $err = implode(" ", $err);
                    $this->session->set_flashdata('error', $err);
                    redirect('home/?error='.$err);
                }
            }else {
                echo $error;
            }
    }

    public function hapusRuangan()
    {
        $id = $_GET['id_ruangan'];

        $query = $this->db->query("delete from ruangan where id_ruangan = '$id'");

        if($query){
            redirect('home');
        }  else {
            redirect('home?err=Gagal menghapus');
        }
    }

    public function konfirmasiPesanan()
    {
        $id = $_GET['id_pesanan'];
        $pesanan = $this->db->query("select * from pesanan where id_ruangan='$id'")->row();

        $query = $this->db->query("update pesanan set status='accepted' where id_pesanan = '$id'");
        $this->db->query("update ruangan set status_ruangan='tidaktersedia' where id_ruangan = '$pesanan->id_ruangan';");

        if($query){
            redirect('home/pesanan');
        }  else {
            redirect('home/pesanan?err=Gagal menghapus');
        }
    }

    public function selesaiPesanan()
    {
        $id = $_GET['id_ruangan'];
        $this->room_model->selesaiPesanan($id);
    }

    public function bayarPesanan()
    {
            $config['upload_path']          = './app-uploads/';
            $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf';
            $config['encrypt_name']         = true;
            $config['max_size']             = '1000'; //kilobyte
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('bukti_pembayaran'))
            {
                    $error = $this->upload->display_errors();
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
                    $bukti_pembayaran = $data['upload_data']['file_name'];
            }

            $data = $this->input->post();
            //unset($data['jumlah']);
            unset($data['bukti_pembayaran']);

            foreach($data as $item=>$value){
                if($value == ""){
                    $err[] = $item . " tidak boleh kosong!";
                }
            }

            $data2 = array('status' => 'paid', 'bukti_pembayaran'=>$bukti_pembayaran
                );
            $data = array_merge($data,$data2);
            if(isset($bukti_pembayaran)){
                if(!isset($err)){
                     $insert = $this->room_model->updatePesanan($data, $_GET['id_pesanan']);
                    if($insert['status']){
                        $this->session->set_flashdata('msg', 'Sukses');
                        redirect('home/pesanan?msg=Pembayaran Berhasil Diproses');
                    }else {
                        $this->session->set_flashdata('error', $insert['error']);
                        redirect('home/pesan?pay&id_pesanan='.$_GET['id_pesanan'].'&error='.$insert['error']);
                    }
                }else {
                    $err = implode(" ", $err);
                    $this->session->set_flashdata('error', $err);
                    redirect('home/pesan?pay&id_pesanan='.$_GET['id_pesanan'].'&error='.$err);
                }
            }else {
                echo $error;
            }
    }

    public function hapusPesanan()
    {
        $id = $_GET['id_pesanan'];

        $pesanan = $this->db->query("select * from pesanan where id_ruangan='$id'")->row();

        $query = $this->db->query("delete from pesanan where id_pesanan = '$id'");
        $this->db->query("update ruangan set status_ruangan='tersedia' where id_ruangan = '$pesanan->id_ruangan'");
        

        if($query){
            redirect('home/pesanan');
        }  else {
            redirect('home/pesanan?err=Gagal menghapus');
        }
    }

    public function pesan()
    {           
        if(empty($this->session->userdata("logged_in"))) {
            redirect('home/login');
        }
                $head['title'] = "Reservasi Tempat Meeting";
                $head['loginPage'] = false;
                $this->load->view('_templates/home_header', $head);

                $data['page'] = 'pesan_view';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }

    public function sendPesanan()
    {
            $config['upload_path']          = './app-uploads/';
            $config['allowed_types']        = 'gif|jpg|jpeg|png|pdf';
            $config['encrypt_name']         = true;
            $config['max_size']             = '1000'; //kilobyte
            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('bukti_pembayaran'))
            {
                    $error = $this->upload->display_errors();
            }
            else
            {
                    $data = array('upload_data' => $this->upload->data());
                    $bukti_pembayaran = $data['upload_data']['file_name'];
            }

            $data = $this->input->post();
            $jumlah = $data['jumlah_orang'];
            //unset($data['jumlah']);
            unset($data['bukti_pembayaran']);

            foreach($data as $item=>$value){
                if($value == ""){
                    $err[] = $item . " tidak boleh kosong!";
                }
            }

            $data2 = array('status' => 'pending', 'bukti_pembayaran'=>$bukti_pembayaran
                );
            $data = array_merge($data,$data2);
            if(isset($bukti_pembayaran)){
                if(!isset($err)){
                     $insert = $this->room_model->tambahPesanan($data);
                     $id = $this->db->insert_id();
                     $this->db->query("update ruangan set status_ruangan='dipesan' where id_ruangan = '".$data['id_ruangan']."'");
                    if($insert['status']){
                        $this->session->set_flashdata('msg', 'Sukses');
                        redirect('home?msg=Pesanan Berhasil Diproses');
                    }else {
                        $this->session->set_flashdata('error', $insert['error']);
                        redirect('home/pesan?id_room='.$_GET['id_room'].'&error='.$insert['error']);
                    }
                }else {
                    $err = implode(" ", $err);
                    $this->session->set_flashdata('error', $err);
                    redirect('home/pesan?id_room='.$_GET['id_room'].'&error='.$err);
                }
            }else {
                echo $error;
            }
    }

    public function pesananBaru()
    {       
            $data = $this->input->post();

            //unset($data['bukti_pembayaran']);

            foreach($data as $item=>$value){
                if($value == ""){
                    $err[] = $item . " tidak boleh kosong!";
                }
            }

            $data2 = array('status' => 'pending'
                );
            $data = array_merge($data,$data2);
            if(true){
                if(!isset($err)){
                     $insert = $this->room_model->tambahPesanan($data);
                     $id = $this->db->insert_id();
                     $this->db->query("update ruangan set status_ruangan='dipesan' where id_ruangan = '".$data['id_ruangan']."'");
                    if($insert['status']){
                        $this->session->set_flashdata('msg', 'Sukses');
                        redirect('home?msg=Pesanan Berhasil Diproses');
                    }else {
                        $this->session->set_flashdata('error', $insert['error']);
                        redirect('home/pesan?id_room='.$_GET['id_room'].'&error='.$insert['error']);
                    }
                }else {
                    $err = implode(" ", $err);
                    $this->session->set_flashdata('error', $err);
                    redirect('home/pesan?id_room='.$_GET['id_room'].'&error='.$err);
                }
            }else {
                echo $error;
            }
    }

    public function registerUser()
    {
            
            $data = $this->input->post();
            //unset($data['jumlah']);

            foreach($data as $item=>$value){
                if($value == ""){
                    $err[] = $item . " tidak boleh kosong!";
                }
            }

            //$data2 = array('status' => 'pending', 'bukti_pembayaran'=>'$bukti_pembayaran'
            //    );
            //$data = array_merge($data,$data2);
            if(!isset($err)){
                 $insert = $this->auth_model->tambahPengguna($data);
                if($insert['status']){
                    $this->session->set_flashdata('msg', 'Sukses');
                    redirect('home/register?msg=Pendaftaran Berhasil');
                }else {
                    $this->session->set_flashdata('error', $insert['error']);
                    redirect('home/register?id_room='.$_GET['id_room'].'&error='.$insert['error']);
                }
            }else {
                $err = implode(" ", $err);
                $this->session->set_flashdata('error', $err);
                redirect('home/register?id_room='.$_GET['id_room'].'&error='.$err);
            }
    }

    public function pesanan()
    {
                $head['title'] = "Reservasi Tempat Meeting";
                $head['loginPage'] = false;
                $this->load->view('_templates/home_header', $head);


                $data['pesanan'] = $this->room_model->getPesanan();
                
                $data['page'] = 'pesanan_view';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);


                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }

    public function login()
    {
                $head['title'] = "Reservasi Tempat Meeting";
                $head['loginPage'] = false;
                $this->load->view('_templates/home_header', $head);

                $data['page'] = 'login_view';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }

    public function register()
    {
                $head['title'] = "Reservasi Tempat Meeting";
                $head['loginPage'] = false;
                $this->load->view('_templates/home_header', $head);

                $data['page'] = 'register_view';
                $data['name'] = $this->config->item("site_name");
                $this->load->view('_templates/content', $data);

                $foot['name'] = $data['name'];
                $this->load->view('_templates/footer', $foot);
    }

}
