<div class="row page-content" style="padding: 20px 20%">
    <h1></h1>
    <?=form_open('home/registerUser');?>
    <?php
            $error = $this->session->flashdata('error');
            if(isset($error)){
        ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error');?></div>
        <?php } ?>
        <div class="form-group">
            <label>Nama<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="nama" value="">
        </div>

        <div class="form-group">
            <label>Jenis Kelamin<span style="color:red">*</span>:</label><br>
            <select class="form-control" name="jenis_kelamin">
                <option>Laki-laki</option>
                <option>Perempuan</option>
            </select>
        </div>

        <div class="form-group">
            <label>Alamat<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="alamat" value="">
        </div>

        <div class="form-group">
            <label>Email<span style="color:red">*</span>:</label><br>
            <input type="email" class="form-control" name="email" value="">
        </div>

        <div class="form-group">
            <label>Nomor Telepon<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="telepon" value="">
        </div>

        <div class="form-group">
            <label>Nama Pengguna<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="username" value="">
        </div>

        <div class="form-group">
            <label>Kata Sandi<span style="color:red">*</span>:</label><br>
            <input type="password" class="form-control" name="password" value="">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Daftar</button>
        </div>
     </form>
</div>