<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$title;?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?=base_url();?>app-assets/css/bootstrap.css">
  <link rel="stylesheet" href="<?=base_url();?>app-assets/css/jquery-ui.css">
  <link rel="stylesheet" href="<?=base_url();?>app-assets/css/styles.css">
  <link rel="stylesheet" href="<?=base_url();?>app-assets/css/font-awesome.min.css">
  <script src="<?=base_url();?>app-assets/js/jquery.min.js"></script>
  <script src="<?=base_url();?>app-assets/js/bootstrap.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?=base_url();?>">Reservasi Tempat Meeting</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="active"><a href="<?=base_url();?>home">Home</a></li>
    <?php if ($this->session->userdata('logged_in')): ?>
      <li><a href="<?=base_url();?>home/pesanan">Daftar Pesanan</a></li>
      <?php if ($this->session->userdata('role') == "Admin"): ?>
        <li><a href="<?=base_url();?>home/tambah_ruangan">Tambah Ruangan</a></li>
      <?php endif;?>
    <?php endif;?>
    </ul>

    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <?php if ($this->session->userdata('logged_in')): ?>
        <li><a><?=ucfirst($this->session->userdata('nama'));?> (<?=$this->session->userdata('role');?>)</a></li>
        <li><a href="<?=site_url('auth\logout');?>">Keluar</a></li>
        <?php else: ?>
        <li><a href="<?=site_url('home\login');?>"> Masuk</a></li>
        <li><a href="<?=site_url('home\register');?>"> Mendaftar</a></li>
        <?php endif;?>
      </ul>
  </div>
</nav>
  
<div class="container text-center">
  <div class="row page-content>