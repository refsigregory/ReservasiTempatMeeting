<div class="row page-content" style="padding: 20px 20%">
    <h1>Detail Pesanan</h1>
    <?=form_open_multipart('/home/tambahRuangan');?>
<?php
            $error = $this->session->flashdata('error');
            if(isset($error)){
        ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error');?></div>
        <?php } ?>
        <div class="form-group">
            <label>Nama Ruangan<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="nama_ruangan">
        </div>

        <div class="form-group">
            <label>Gambar<span style="color:red">*</span>:</label><br>
            <input type="file" class="form-control" name="gambar">
        </div>


        <div class="form-group">
            <label>Deskripsi<span style="color:red">*</span>:</label><br>
            <textarea name="deskripsi" class="form-control"></textarea>
        </div>

        <div class="form-group">
            <label>Alamat<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="alamat">
        </div>

        <div class="form-group">
            <label>Kapasitas<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="kapasitas" value="0">
        </div>

         <div class="form-group">
            <label>Harga<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="harga" value="0">
        </div>


        <button type="submit" class="btn btn-primary btn-block">Tambah</button>
    </form>
  </div>
</div>