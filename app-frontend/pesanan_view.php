        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="panel panel-default">
                      <div class="panel-heading"><b>Daftar Pesanan</b>
                      </div> 
                      <div class="panel-body">
                     
<?php $no = 1; ?>
<table id="datatable" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Pemesan</th>
                <th>No HP</th>
                <th>Alamat</th>
                <th>Email</th>
                <th>Nama acara</th>
                <th>Jumlah Orang</th>
                <th>Tanggal Pemesanan</th>
                <th>Waktu Pemesanan</th>
                <th>Status</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        <?php if($pesanan != ""): ?>
            <?php foreach ($pesanan as $row): ?>
            <tr>
                <td><?=$no;?></td>
                <td><?=$row->nama;?></td>
                <td><a href="tel:<?=$row->telepon;?>"><?=$row->telepon;?></a></td>
                <td><?=$row->alamat;?></td>
                <td><?=$row->email;?></td>
                <td><?=$row->nama_acara;?></td>
                <td><?=$row->jumlah_orang;?></td>
                <td>
                    <?=$row->tanggal_pemesanan;?>
                </td>
                <td>
                    <?=$row->waktu_pemesanan;?> <?=$row->jam_pemesanan;?>
                </td>
                <td>
                    <?php
                        if($row->status == 'pending'){
                            echo "Menunggu Pembayaran";
                        }else if($row->status == 'paid'){
                            echo "Menunggu Konfirmasi";
                        }else if($row->status == 'accepted') {
                            echo "Pesanan Dikonfirmasi";
                        } else {
                            echo $row->status;
                        }
                    ?>
                </td>
                <td>
                    <?php if($this->session->userdata('role') == "Admin"): ?>
                        <a href="<?=base_url();?>app-uploads/<?=$row->bukti_pembayaran;?>" class="btn btn-small btn-info" target="_blank">Lihat Bukti Pembayaran</a>
                        <?php if($row->status == 'paid'):?>
                        <a href="<?=base_url();?>home/konfirmasiPesanan/?id_pesanan=<?=$row->id_pesanan;?>" class="btn btn-small btn-primary" onclick="return Tanya();">Konfirmasi Pesanan</a>
                        <?php endif;?>

                        <?php if($row->status == 'accepted'):?>
                            <?php if($row->status_ruangan != 'tersedia'):?>
                                <a href="<?=base_url();?>home/selesaiPesanan/?id_ruangan=<?=$row->id_ruangan;?>" class="btn btn-small btn-success" onclick="return Tanya();">Selesai Pesanan</a>
                            <?php else: ?>     
                                <a href="<?=base_url();?>home/hapusPesanan/?id_pesanan=<?=$row->id_pesanan;?>" class="btn btn-small btn-danger" onclick="return Tanya();">Batalkan Pesanan</a>
                            <?php endif;?>
                        <?php endif;?>
                    <?php else:?>
                        <?php if($row->status == 'pending'):?>
                            <?php if($row->status_ruangan != 'tersedia'):?>
                                <a href="<?=base_url();?>home/pesan/?pay&id_pesanan=<?=$row->id_pesanan;?>" class="btn btn-small btn-success">Bayar Pesanan</a>    
                                <a href="<?=base_url();?>home/hapusPesanan/?id_pesanan=<?=$row->id_pesanan;?>" class="btn btn-small btn-danger" onclick="return Tanya();">Batalkan Pesanan</a>
                            <?php endif;?>
                        <?php endif;?>
                    <?php endif;?>
                </td>
            </tr>
        <?php
            $no++;
            endforeach;
        ?>
      <?php endif;?>
        </tbody>
    </table>

 </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->


<script type="text/javascript">
    $(document).ready(function() {
        $('#datatable').DataTable();
    } );

    function Tanya()
    {
        var tanya = confirm("Anda yakin ini?");

        if(tanya == true)
            {
                return true;
            }else {
                return false;
            }
    }
</script>