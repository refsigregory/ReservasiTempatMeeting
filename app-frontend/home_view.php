<center class="row page-content" style="padding-top: 20px">
    <h1>Daftar Ruangan</h1>
    <?php
    if($ruangan != ""):
    foreach($ruangan as $row):?>
    <div class="col-md-4">
        <div class="panel panel-default">
          <div class="panel-body">
            <?php if($row->gambar == NULL): ?>
              <img src="<?=base_url();?>app-assets/images/no-picture.jpg" style="width: 80%;border:solid 1px" alt="" />
          <?php else: ?>
                <img src="<?=base_url();?>app-uploads/<?=$row->gambar;?>" style="width: 80%;border:solid 1px" alt="" />
          <?php endif;?>
          </div>
            <div class="panel-heading">
             <div class="pull-left"><i class="fa fa-map-marker"></i> <?=$row->alamat;?></div>
             <div class="pull-right"><i class="fa fa-user"></i>  <?=$row->kapasitas;?> orang</div>
             <div class="clearfix"></div>
           </div>

          <div class="panel-heading"><b><?=$row->nama_ruangan;?></b> 
              <?php if($row->status_ruangan == 'dipesan'):?>
                <span class="label label-danger"><?=$row->status_ruangan;?></span>
              <?php else:?>
                <span class="label label-success"><?=$row->status_ruangan;?></span>
              <?php endif;?>
            <br></div>  
          <div class="panel-heading">
            Rp.<?=number_format($row->harga);?> / orang<br><br>
            <?php if($this->session->userdata('role') == "Admin"):?>
              <a class="btn btn-danger" href="?view=<?=$row->id_ruangan;?>">Hapus</a>
            <?php else: ?>
              <a class="btn btn-primary" href="?view=<?=$row->id_ruangan;?>">Pesan Sekarang</a>
            <?php endif;?>
          </div>
        </div>
    </div>
<?php endforeach; else:?>
Tidak ada data.
<?php endif;?>
</center>