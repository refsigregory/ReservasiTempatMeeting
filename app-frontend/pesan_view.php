<div class="row page-content" style="padding: 20px 20%">
    <?php if(isset($_GET['order'])):?>
    <?php 
        $harga = $this->room_model->getroomByID($this->input->get('id_ruangan'))->harga;
        $kapasitas = $this->room_model->getroomByID($this->input->get('id_ruangan'))->kapasitas;
        $jumlah_orang = $this->input->post('jumlah_orang');
        $waktu_pemesanan = $this->input->post('waktu_pemesanan');
        $jam_pemesanan = $this->input->post('jam_pemesanan');

        if($waktu_pemesanan == 'fullday') {
            $total_harga = ($harga*2) * $jumlah_orang;
        } else {
            $total_harga = $harga * $jumlah_orang;
        }


        if($jumlah_orang > $kapasitas) {
            redirect('/home/pesan?id_ruangan=' . $_GET['id_ruangan'] . '&err=Kapasitas tidak cukup');
        }

        if($this->db->query("select * from pesanan where id_ruangan = '".$this->input->get('id_ruangan')."' and tanggal_pemesanan = '".$this->input->post('tanggal_pemesanan')."' and waktu_pemesanan = 'halfday' AND jam_pemesanan = '$jam_pemesanan'")->num_rows() > 0 || $this->db->query("select * from pesanan where id_ruangan = '".$this->input->get('id_ruangan')."' and tanggal_pemesanan = '".$this->input->post('tanggal_pemesanan')."' and waktu_pemesanan = 'fullday'")->num_rows() > 0 ) 
        { // ada halfday? || ada fullday
            redirect('/home/pesan?id_ruangan=' . $_GET['id_ruangan'] . '&err=Ruangan sudah dipesan');
        }
    ?>
    <?=form_open_multipart('/home/pesananBaru?id_ruangan=' . $_GET['id_ruangan']);?>
    <input type="hidden" name="id_ruangan" value="<?=$_GET['id_ruangan'];?>">
    <input type="hidden" name="id_pengguna" value="<?=$this->session->userdata("id_pengguna");?>">
    <input type="hidden" name="nama_acara" value="<?=$this->input->post('nama_acara');?>">
    <input type="hidden" name="tanggal_pemesanan" value="<?=$this->input->post('tanggal_pemesanan');?>">
    <h1>Detail Pesanan</h1>
        <div class="form-group">
            <label>Jumlah Orang<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="jumlah_orang" value="<?=$jumlah_orang;?>" readonly="">
        </div>

        <div class="form-group">
            <label>Waktu Pemesanan<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="waktu_pemesanan" value="<?=$waktu_pemesanan;?>" readonly="">
            
            <?php if($waktu_pemesanan != 'fullday'):?>
            <br>
            <input type="text" class="form-control" name="jam_pemesanan" value="<?=$jam_pemesanan;?>" readonly="">
            <?php endif;?>
        </div>

        <div class="form-group">
            <label>Harga Ruangan <?=$waktu_pemesanan;?> (per orang)<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="total_harga" value="<?=number_format($harga);?>" disabled="">
            <label>Total Harga<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="total_harga" value="<?=number_format($total_harga);?>" readonly="">
        </div>

    <button type="submit" class="btn btn-primary">Konfirmasi Pesanan</button>
    <a  href="<?=base_url();?>" class="btn btn-danger">Batalkan Pesanan</a>
    </div>
    <?php elseif(isset($_GET['pay'])):?>
    <?=form_open_multipart('/home/bayarPesanan?id_pesanan=' . $_GET['id_pesanan']);?>

    <input type="hidden" name="id_pesanan" value="<?=$_GET['id_pesanan'];?>">
    <h1>Detail Pembayaran</h1>
        <div class="form-group">
            Bank: <br>
            Nomor Rekening: <br>
            Nama :<br>
        </div>

        <div class="form-group">
            <label>Bukti Pembayaran<span style="color:red">*</span>:</label><br>
            <input type="file" class="form-control" name="bukti_pembayaran">
        </div>
    <button type="submit" class="btn btn-primary">Bayar Sekarang</button>
    </div>
    <?php else:?>
    <h1>Pemesanan Ruangan</h1>
    <?=form_open_multipart('/home/pesan?order&id_ruangan=' . $_GET['id_ruangan']);?>
    <?php
            $error = $this->session->flashdata('error');
            if(isset($error)){
        ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error');?></div>
        <?php } ?>
        <div class="form-group">
            <label>Nama Ruangan<span style="color:red">*</span>:</label><br>
            <?=$this->room_model->getroomByID($this->input->get('id_ruangan'))->nama_ruangan;?>
        </div>
        <div class="form-group">
            <label>Kapasitas<span style="color:red">*</span>:</label><br>
            <?=$this->room_model->getroomByID($this->input->get('id_ruangan'))->kapasitas;?> orang
        </div>

        <div class="form-group">
            <label>Nama Acara<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="nama_acara">
        </div>

        <div class="form-group">
            <label>Jumlah Orang<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="jumlah_orang" value="0">
        </div>

        <div class="form-group">
            <label>Tanggal Pemesanan<span style="color:red">*</span>:</label><br>
            <select name="tanggal_pemesanan" class="form-control">
              <?php for ($tgl = 0; $tgl <= 14; $tgl++): ?>
                  <option><?php $tanggal = date("Y-m-d"); echo date('Y-m-d', strtotime($tanggal. ' + '. $tgl  .' days'));?></option>
              <?php endfor;?>
            </select>
        </div>

        <div class="form-group">
            <label>Waktu Pemesanan<span style="color:red">*</span>:</label><br>
            <select name="waktu_pemesanan" class="form-control">
                  <option>halfday</option>
                  <option>fullday</option>
            </select>
            <br>
            <select name="jam_pemesanan" class="form-control">
                <option>pagi</option>
                <option>siang</option>
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Pesan Sekarang</button>
    </form>        
    <?php endif;?>
  </div>
</div>