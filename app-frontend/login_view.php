<div class="row page-content" style="padding: 20px 20%">
    <h1></h1>
    <?=form_open('/auth/checkLogin');?>
    <?php
            $error = $this->session->flashdata('error');
            if(isset($error)){
        ?>
            <div class="alert alert-danger"><?php echo $this->session->flashdata('error');?></div>
        <?php } ?>

        <div class="form-group">
            <label>Nama Pengguna<span style="color:red">*</span>:</label><br>
            <input type="text" class="form-control" name="username" value="">
        </div>
        <div class="form-group">
            <label>Kata Sandi<span style="color:red">*</span>:</label><br>
            <input type="password" class="form-control" name="password" value="">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary btn-block">Masuk</button>
        </div>
     </form>
</div>